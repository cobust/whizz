package com.example.whizz.whizz;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

/**
 * Created by Cobus Truter on 1/22/2017.
 */

public class RejectCall extends BroadcastReceiver{
    @Override
    public void onReceive(Context context, Intent intent) {
        String number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);
        setResultData(null);
        Toast.makeText(context, number, Toast.LENGTH_LONG).show();
        /*
        if (number.equals("*111#")){
            Toast.makeText(context, number, Toast.LENGTH_LONG).show();
            setResultData(null);
        }*/
    }
}