package com.example.whizz.whizz;

/**
 * Created by Cobus Truter - 2017
 */

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.content.Intent;

public class LoginActivity extends AppCompatActivity {
    public final static String EXTRA_MESSAGE = "com.example.whizz.whizz.MESSAGE";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        Button login_button = (Button) findViewById(R.id.login_button);
        login_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LoginActivity.this.login(v);
            }
        });
    }

    public void login(View view){
        EditText username = (EditText) findViewById(R.id.login_id);
        EditText password = (EditText) findViewById(R.id.login_password);
        Intent intent = new Intent(this, MainActivity.class);
        String message = username.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}
