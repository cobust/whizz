package com.example.whizz.whizz;

/**
 * Created by Cobus Truter - 2017
 */

import android.animation.IntArrayEvaluator;
import android.content.Intent;
import android.content.res.Resources;
import android.media.Image;
import android.os.Bundle;
import android.os.StrictMode;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.SwitchCompat;
import android.util.TypedValue;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;


import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

public class MainActivity extends AppCompatActivity {

    TextView tempDigits, degreeSymbol;
    ImageView degree_ring;
    Animation rotateClkInf;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }

        //Intent message handling
        Intent intent = getIntent();
        String message = intent.getStringExtra(LoginActivity.EXTRA_MESSAGE);

        // Init Toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_menu);
        setSupportActionBar(toolbar);

        // Init random variables
        connOpenWRT();
        tempDigits = (TextView) findViewById(R.id.temp_digits);
        degreeSymbol = (TextView) findViewById(R.id.degree_symbol);
        degreeSymbol.setText((char) 0x00B0 + "C");

        // Infinite rotation of degree ring when geyser is active
        degree_ring = (ImageView) findViewById(R.id.degree_circle_ring);
        rotateClkInf = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.rotate_clockwise_infinite);
        degree_ring.startAnimation(rotateClkInf);

        // Init floating action button
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        // Manual override switch
        SwitchCompat manualOverride = (SwitchCompat) findViewById(R.id.geyser_manual_override);
        manualOverride.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener(){
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                ImageView thermo = (ImageView) findViewById(R.id.thermometer);
                ImageView degree_ring_bg = (ImageView) findViewById(R.id.degree_ring_bg);
                TextView onOffText = (TextView) findViewById(R.id.on_off_text);

                if (isChecked){
                    // Manual override ON - System OFF
                    System.out.println("Checked");
                    makeGrey(thermo);
                    makeGrey(degree_ring_bg);
                    makeGrey(degree_ring);
                    onOffText.setText("OFF");
                    setTemp(20);
                    rotateClkInf.cancel();
                    rotateClkInf.reset();
                } else{
                    // Manual override OFF - System ON
                    System.out.println("Unchecked");
                    resetGrey(thermo);
                    resetGrey(degree_ring);
                    resetGrey(degree_ring_bg);
                    onOffText.setText("ON");
                    setTemp(63);
                    degree_ring.startAnimation(rotateClkInf);
                }
            }
        });

        System.out.println("Test");
        MainActivity.this.createTemperatureBubbles();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_refresh:
                startActivity(new Intent(this, viewSchedule.class));
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void setTemp(int temp){
        // Sets temperature text on main screen
        // Only integers allowed due to space restrictions
        TextView tempDigits = (TextView) findViewById(R.id.temp_digits);
        tempDigits.setText(Integer.toString(temp));
    }


    public void fade(View view){
        ImageView thermo = (ImageView) findViewById(R.id.thermometer);
        Animation fadeAnim = AnimationUtils.loadAnimation(getApplicationContext(),R.anim.fade);
        thermo.startAnimation(fadeAnim);
    }

    public static void makeGrey(ImageView v)
    {
        ColorMatrix matrix = new ColorMatrix();
        matrix.setSaturation(0);
        ColorMatrixColorFilter cf = new ColorMatrixColorFilter(matrix);
        v.setColorFilter(cf);
        v.setImageAlpha(128);
    }

    public static void resetGrey(ImageView v){
        v.setColorFilter(null);
        v.setImageAlpha(255);
    }

    private void createTemperatureBubbles(){
        // Lowest point for min_bubble is 50dp, highest is 330dp

        int scale_lowest_temp = 0;
        int scale_highest_temp = 100;

        int lowest_location_dp = 58;
        int highest_location_dp = 339;

        // conversionFactor is the amount of dp per temperature unit
        double conversionFactor = (double)(highest_location_dp - lowest_location_dp)/(double)(scale_highest_temp - scale_lowest_temp);

        // Testing temperatures
        int testTemp = 100;
        double dpBottomMargin = conversionFactor*testTemp + lowest_location_dp;
        System.out.println(dpBottomMargin);

        Resources resources = getResources();
        int pxBottomMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, (int)dpBottomMargin,resources.getDisplayMetrics());

        // Setting new margin
        ImageView min_bubble = (ImageView) findViewById(R.id.min_temp_bubble);
        RelativeLayout.LayoutParams min_bubble_layout = (RelativeLayout.LayoutParams) min_bubble.getLayoutParams();
        min_bubble_layout.bottomMargin = pxBottomMargin;

        //Setting text
        TextView minTempText = (TextView) findViewById(R.id.minTempText);
        minTempText.setText(""+testTemp +(char) 0x00B0 + "C");
    }

    public void connOpenWRT()
    {
        System.out.println("Beginning Auth");
        try
        {
            JSONRPCClient client = JSONRPCClient.create("http://192.168.56.1:8080"+
                    "/ubus", JSONRPCParams.Versions.VERSION_2);
            System.out.println("Client created.");
            client.setConnectionTimeout( 5000 );
            client.setSoTimeout( 5000 );

            System.out.println("Logging in...");
            JSONObject jsonObj = new JSONObject();
            //JSONObject result = new JSONObject();
            //jsonObj.put("00000000000000000000000000000000","");
            //jsonObj.put("session", "");
            //jsonObj.put("login", "");
            jsonObj.put("username", "root");
            jsonObj.put("password", "Awe123456");
            //System.out.println(jsonObj.toString());
            Object result = client.call("call", "00000000000000000000000000000000", "session", "login", jsonObj );
            System.out.println("Login process completed.");
            System.out.println("Result: " + result);
            JSONObject resultObj = ((JSONArray)result).getJSONObject(1);
            String session = resultObj.get("ubus_rpc_session").toString();
            System.out.println("Get result: " + session);

            result = client.call("call", session, "network.interface.wan", "status", new JSONObject());
            System.out.println("Result: " + result);
            //resultObj = ((JSONArray)result).getJSONObject(1);
            //System.out.println("Result obj: " + resultObj);
            /* client = JSONRPCClient.create("http://192.168.56.1:8080"+
                    "/ubus", JSONRPCParams.Versions.VERSION_2);
            System.out.println("Client created.");
            client.setConnectionTimeout( 5000 );
            client.setSoTimeout( 5000 );

            System.out.println("Logging in...");
            String token = client.callString("login", "root", "Awe123456" );

            //System.out.println(client.callJSONObject("login", jsonObj));
            System.out.println("Login process completed.");
            System.out.println("Token: " + token);*/
            //SSystem.out.println("Token: " + test.toString());

            //Test auth
            /*
            System.out.println("Login test process with TOKEN started.");
            JSONRPCClient client1 = JSONRPCClient.create("http://192.168.56.1:8080" +
                    "/cgi-bin/luci/rpc/uci?auth="+token, JSONRPCParams.Versions.VERSION_2);
            System.out.println("Client created with auth token.");
            client1.setConnectionTimeout( 2000 );
            client1.setSoTimeout( 2000 );
            //String param1 = "config: network"
            //String hostname = client.callString("uptime");
            String hostname = client.callString("get", "wireless.radio0.hwmode" );
            System.out.print("Tested auth, hostname: " + hostname);*/

        }catch (JSONRPCException rpcEx) {
            System.out.println("rpcEx");
            rpcEx.printStackTrace();
        } catch (Exception e) { //JSONException jsEx
           System.out.println("Exception:" + e.toString());
           // jsEx.printStackTrace();
        }
    }

    /*
    public void TestAuth()
    {
        try
        {
            JSONRPCClient client = JSONRPCClient.create("http://10.0.0.7" +
                    "/cgi-bin/luci/rpc/uci?auth="+WRTControlActivity.Token, JSONRPCParams.Versions.VERSION_2);
            client.setConnectionTimeout( 2000 );
            client.setSoTimeout( 2000 );
            String hostname = client.callString("get", "wireless.radio0.hwmode" );
            Log.v(WRTControlActivity.CFG_NAME, hostname );
            return hostname;
        }catch( Exception e )
        {
        }
    }*/


    /*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }*/
}
